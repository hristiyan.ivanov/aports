# Contributor: Adam Bruce <adam@adambruce.net>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=oci-cli
pkgver=3.33.1
pkgrel=0
pkgdesc="Oracle Cloud Infrastructure CLI"
url="https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm"
arch="noarch"
license="UPL-1.0 OR Apache-2.0"
depends="
	python3
	py3-arrow
	py3-certifi
	py3-click
	py3-cryptography
	py3-dateutil
	py3-jmespath
	py3-oci
	py3-openssl
	py3-prompt_toolkit
	py3-six
	py3-terminaltables
	py3-tz
	py3-yaml
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/oracle/oci-cli/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # Cannot test as OCI resource identifiers are required as environment variables

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	find "$pkgdir"/usr/lib/python* -type d -name "tests" -exec rm -r {} \+
}

sha512sums="
41180d58e4b6a414dc4a277e449a110f7f35cc9eeb5e9b90e0f95a27f67f0ee84d1efd6581ec991f7e4b48721d86ca5af5b09461e50af8ea3fc6b591025fe3e5  oci-cli-3.33.1.tar.gz
"
