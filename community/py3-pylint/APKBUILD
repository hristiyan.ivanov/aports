# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pylint
pkgver=2.17.6
pkgrel=0
pkgdesc="Analyzes Python code looking for bugs and signs of poor quality"
url="https://github.com/PyCQA/pylint"
arch="noarch !s390x" # py3-dill
license="GPL-2.0-or-later"
depends="
	py3-astroid
	py3-dill
	py3-isort
	py3-mccabe
	py3-platformdirs
	py3-tomlkit
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-gitpython
	py3-py
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-runner
	py3-pytest-timeout
	py3-pytest-xdist
	py3-requests
	py3-typing-extensions
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/PyCQA/pylint/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir"/pylint-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest --benchmark-disable tests
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/pylint-*.whl
}

sha512sums="
1e1bba425d295f9d267ef439f4f402dfc7284ee57ac41c222ac39e502c66bbac60c9afb0024ca4236de7cc7130ac733c781f5ee98670fae054005a8d69107123  py3-pylint-2.17.6.tar.gz
"
