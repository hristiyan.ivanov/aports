# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-icalendar
_pyname=icalendar
pkgver=5.0.9
pkgrel=0
pkgdesc="icalendar parser library for Python"
url="https://github.com/collective/icalendar"
arch="noarch"
license="BSD-2-Clause"
depends="py3-dateutil py3-tz"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-hypothesis tzdata"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/i/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest -v "src/$_pyname/tests"
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl

	cd "$pkgdir"
	rm -r usr/lib/python*/site-packages/icalendar/tests/
}

sha512sums="
b70c1adbcaeebbd81ba8c4cf4db146d593131dd3ba7c995f3b2417bf871cca82042ec43eab127f158895070d4e17ebf46cb30aca0a33b8344665798c29fd2e27  icalendar-5.0.9.tar.gz
"
